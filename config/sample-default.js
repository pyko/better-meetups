// Rename this file to 'default.js'
// Get your API Key here: https://secure.meetup.com/meetup_api/key/

var defaultConfig = {
   meetup: {
       key: 'YOUR API KEY HERE'
   }
 };

module.exports = defaultConfig;
