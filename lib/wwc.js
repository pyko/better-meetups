var meetupApi = require('meetup-api');
var config = require('config');
var q = require('q');
var _ = require('lodash');
var moment = require('moment');
var meetup = meetupApi({key: config.meetup.key});
var util = require('util');

function getEvents() {
  var deferred = q.defer();
  meetup.getEvents({
    "group_urlname": "Women-Who-Code-Sydney",
    "status": "upcoming,past",
    "page": 3,
    "desc": true
  }, function (error, response) {
    if (error) {
      deferred.reject(error);
    } else {
      var events = response.results.map(function(event) {
                      return { "id": event.id,
                               "name": event.name,
                               "time": event.time};
                    });
      console.log('Sample event: ', events[0]);
      deferred.resolve(events);
    }
  });
  return deferred.promise;
}

function getTallies(event) {
 var deferred = q.defer();

 meetup.getRSVPs({
   "event_id": event.id,
   "page": 1,
 }, function(error, response) {
   if (error) {
     deferred.reject(error);
   } else {
     deferred.resolve({ 'event' : event,
                        'tallies' : response.results[0].tallies });
   }
 });
 return deferred.promise;
}

function getDropouts(event) {
  var deferred = q.defer();

  meetup.getRSVPs({
    "event_id": event.id,
    "page": 200,
    "rsvp": "no"
  }, function(error, response) {
    if (error) {
      deferred.reject(error);
    } else {
      var modifiedRsvps = _.filter(response.results, function(person){
        return person.mtime !== person.created;
      });
      var dropouts = _.filter(modifiedRsvps, function(person){
        return moment(person.mtime) > moment(person.event.time).subtract(36, 'hour');
      });
      dropouts = dropouts.map(function(dropout) {
        var end = moment(dropout.event.time);
        var startTime = moment(dropout.mtime);
        var hours = moment.duration(end.diff(startTime)).asHours();

        return {'member': dropout.member,
                'mtime': dropout.mtime,
                'hours': hours };
      });
      deferred.resolve({ 'event' : event,
                         'dropouts' : dropouts });
    }
  });
  return deferred.promise;
}

function getNoshows(event) {
  var deferred = q.defer();
  meetup.getEventAttendance({
    "urlname": "Women-Who-Code-Sydney",
    "id": event.id,
    "page": 200,
    "filter": "yes"
  }, function(error, response) {
    if (error) {
      deferred.reject(error);
    } else {
      var noShows = _.filter(response, function(person){
        return person.status === 'absent' || person.status === 'noshow';
      });
      noShows = noShows.map(function(noShow){
        return {
          'member': noShow.member
        };
      });

      deferred.resolve({ 'event' : event,
                         'noShows' : noShows });
    }
  });
  return deferred.promise;
}

function zipEvents(results) {
  var tempEvents = {};
  for (var result of results) {
    if (!(result.event.id in tempEvents)) {
      tempEvents[result.event.id] = {
        event: result.event
      };
    }
    if (result.noShows) {
      tempEvents[result.event.id].noShows = result.noShows;
    }
    if (result.dropouts) {
      tempEvents[result.event.id].dropouts = result.dropouts;
    }
  }
  var zippedResults = [];
  for(var id in tempEvents) {
    zippedResults.push(tempEvents[id]);
  }
  zippedResults = _.sortBy(zippedResults, function(result) {
    return result.event.id;
  });
  zippedResults = _(zippedResults).reverse().value();
  return zippedResults;
}

function getBadPeople() {
  var deferred = q.defer();
  getEvents().then(function (events) {
    var allDropouts = events.map(function(event) {
      return getDropouts(event);
    });
    var allNoShows = events.map(function(e) {
      return getNoshows(e);
    });
    q.all(allNoShows.concat(allDropouts)).then(function(results) {
      deferred.resolve(zipEvents(results));
    }).done();
  }).fail(function (err) {
    console.log('err', err);
    deferred.reject(err);
  }).done();

  return deferred.promise;
}

function getAttendance() {
  var deferred = q.defer();

  getEvents().then(function (events) {
    var allRSVPs = events.map(function(event) {
      return getTallies(event);
    });
    q.all(allRSVPs).then(function(results) {
      deferred.resolve(results);
    }).done();
  }).fail(function (err) {
    deferred.reject(err);
  }).done();

  return deferred.promise;
}

module.exports = {
  'getEvents': getEvents,
  'getBadPeople': getBadPeople,
  'getAttendance': getAttendance
};