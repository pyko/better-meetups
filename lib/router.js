var wwc = require('./wwc');
var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  res.render('home');
});


router.get('/events', function(req, res) {

  wwc.getAttendance().then(function (result) {
      res.render('events', {events: result});
      // res.status(200)
      //   .send(result);
  }, function (err) {
      res.status(500).send(err);
  });

});

router.get('/bad-people', function(req, res) {
  wwc.getBadPeople().then(function (results) {
    res.render('bad-people', {results: results});
      // res.status(200)
      //   .send(result);
  }, function (err) {
      res.status(500).send(err);
  });

});

module.exports = router;
