'use strict';
var express = require('express');
var bodyParser = require('body-parser');
var router = require('./lib/router');
var exphbs = require('express-handlebars');
var port = 3000;

function errorHandler(err, req, res, next) {
	console.log(err);
	res.send('Something broke :(');
}

var app = express();
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');
app.use(express.static('assets'));

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use('/', router);
app.use(errorHandler);

app.listen(port);
console.log('Express server listening on port %s', port);

